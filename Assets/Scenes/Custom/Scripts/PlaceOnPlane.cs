﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace RikuTheFuffs
{
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceOnPlane : MonoBehaviour
    {
        ARRaycastManager raycastManager;
        List<ARRaycastHit> hits; //just for caching purposes
        [SerializeField]
        GameObject prefabToPlace;


        void Start()
        {
            raycastManager = GetComponent<ARRaycastManager>();
            hits = new List<ARRaycastHit>();
        }

        void Update()
        {
            //do some raycasting when you touch a place
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    //do a raycast on a specific point
                    if (raycastManager.Raycast(touch.position, hits, TrackableType.PlaneWithinPolygon))
                    {
                        Pose pose = hits[0].pose;
                        Instantiate(prefabToPlace, pose.position, pose.rotation);
                    }
                }
            }
        }
    }
}